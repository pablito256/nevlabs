function initCounter(){
	$('.product__counter').on('click','.product__counter-up',function(e){
		e.preventDefault();
		var input = $(this).parent().find('.product__counter-input');
		if(input.val() < 100){
			var newCount = Number(input.val()) + 1;
			input.val(newCount);
		}
		checkChange(input);
	});
	$('.product__counter .product__counter-down').on('click',function(e){
		e.preventDefault();
		var input = $(this).parent().find('.product__counter-input');
		if(input.val() > 1){
			var newCount = Number(input.val()) - 1;
			input.val(newCount);
		}
		checkChange(input);
	});
	$('.product__counter .product__counter-input').on('input',function(){
		if (this.value == 0 ){
			this.value = 1;
		}else if(this.value > 100){
			this.value = 100;
		}else if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
	});
	function checkChange(input){
		if(input.val() > 1){
			input.parent().find('.product__counter-down').addClass('active');
		}
		else if(input.val() == 1){
			input.parent().find('.product__counter-down').removeClass('active');
		}
	}
}
$(document).ready(function(){
	initCounter();
});