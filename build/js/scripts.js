/*
 * sdToggle plugin v1.5
 */

;(function($){
    // значение по умолчанию
    var defaults = {
        parentOpenClass:'active',
        openLink:'span.open',
        closeLink:'span.close',
        target:'.slide',
        firstOpen:false,
        closeOther:true,
        speed: 500
    };

    var methods = {
        init:function(params) {
            var options = $.extend({}, defaults, params);
            var sliders = this;
            return this.each(function(i) {
                var $this = $(this);
                if(!options['firstOpen'] || (options['firstOpen'] && i>0) ) {
                    if(!$this.hasClass(options['parentOpenClass'])) $(options['target'],$this).hide();
                }

                if(options['openLink'] !=options['closeLink']){
                    $(options['openLink'],$this).bind('click', function() {
                        methods.showBlock($this,options,sliders);
                        return false;
                    });
                    $(options['closeLink'],$this).bind('click', function() {
                        methods.hideBlock($this,options);
                        return false;
                    });
                }
                else{
                    $(options['openLink'],$this).bind('click', function() {
                        methods.toggleBlock($this,options,sliders);
                        return false;
                    });
                }
            });
        },
        showBlock:function(parent,options,sliders){
            $(options['target'],parent).slideDown(options['speed']);
            parent.addClass(options['parentOpenClass']);
            if(options['closeOther']){
                $(sliders).not(parent).removeClass(options['parentOpenClass']).find(options['target']).slideUp(options['speed']);
            }
        },
        hideBlock:function(parent,options){
            $(options['target'],parent).slideUp(options['speed']);
            parent.removeClass(options['parentOpenClass']);
        },
        toggleBlock:function(parent,options,sliders){
            if($(options['target'],parent).is(":hidden")) this.showBlock(parent,options,sliders);
            else this.hideBlock(parent, options);
        }
    };

    $.fn.sdToggle = function(method){
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод "' +  method + '" не найден в плагине jQuery.sdToggle' );
        }
    };

}) (jQuery);